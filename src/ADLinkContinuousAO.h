#ifndef _ADLinkContinuousAO_H
#define _ADLinkContinuousAO_H
//============================================================
//DEPENDENCIES
//============================================================
#include <Tango.h>
#include <asl/ContinuousAO.h>


class ADLinkContinuousAO: public asl::ContinuousAO, public Tango::LogAdapter
{
    
public: 
  
  ADLinkContinuousAO (Tango::DeviceImpl* dev);
  virtual ~ADLinkContinuousAO (void) ;  
  /**
  * Receive errors ocurring during the waveform generation.
  * @param de The error
  */
  virtual void handle_error (const asl::DAQException& de);
  /**
  * Get the mutex
  */
  inline void lock_data(void)
  {
    data_lock_.acquire();
  };
  /**
  * Release the mutex
  */
  inline void unlock_data(void)
  {
    data_lock_.release();
  };
  unsigned long err_ctr;
private:
	ACE_Thread_Mutex data_lock_;
};
#endif	// _ADLinkContinuousAO_H
