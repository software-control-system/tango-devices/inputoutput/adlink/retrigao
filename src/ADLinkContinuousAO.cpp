//============================================================
//DEPENDENCIES
//============================================================
#include <asl/ASLExceptionsHandler.h>
#include "ADLinkContinuousAO.h"

//============================================================
//ADLinkContinuousAO::ADLinkContinuousAO
//============================================================
ADLinkContinuousAO::ADLinkContinuousAO (Tango::DeviceImpl* dev) 
    : Tango::LogAdapter(dev),
    asl::ContinuousAO (),
    err_ctr(0)
{
    
} 
//============================================================
//ADLinkContinuousAO::~ADLinkContinuousAO
//============================================================
ADLinkContinuousAO::~ADLinkContinuousAO (void) 
{   
}
//============================================================
//ADLinkContinuousAO::handle_error
//============================================================
void ADLinkContinuousAO::handle_error (const asl::DAQException& de)
{
  cout<<"error occured during acquisition"<<endl;
  lock_data();
  err_ctr++;
  unlock_data();
  _ASL_TO_TANGO_EXCEPTION(de, df);
  ERROR_STREAM<<"error occured during acquisition"<<endl;
  ERROR_STREAM<<df<<endl;
}
